import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Auth } from './_helpers/auth';
import { EnCurso } from './_helpers/en-curso'

const routes: Routes = [
  { 
    path: 'sesion', 
    loadChildren: './modules/sesion/sesion.module#SesionModule' 
  },
  {
    path: '',
    loadChildren: './modules/noticias/noticias.module#NoticiasModule',
    canActivate: [Auth,EnCurso]
  },
  {
    path: 'noticias',
    loadChildren: './modules/noticias/noticias.module#NoticiasModule',
    canActivate: [Auth,EnCurso]
  },
  {
    path: 'asistencias',
    loadChildren: './modules/asistencias/asistencias.module#AsistenciasModule',
    canActivate: [Auth]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
