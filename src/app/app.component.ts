import { Component } from '@angular/core';
import { AsistenciasService } from './_services/asistencias.service';
import { AutenticarService } from './_services/autenticar.service';
import { Sesion } from './_models/sesion';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public title = 'rescueapp';
  public usuarioLogueado: boolean;
  public usuario: Sesion;
  public asistenciaEnCurso: boolean = false;

  constructor(
    private autenticarService: AutenticarService,
    private router: Router,
    private asistenciasService: AsistenciasService,
  ) {
    this.autenticarService.sesion$.subscribe(x => {
      this.usuarioLogueado = x !== null;
      if(this.usuarioLogueado)
        this.autenticarService.usuario$.subscribe(usuario => {
          this.usuario = usuario;
        });
    });
    this.asistenciasService.asistenciaEnCurso$.subscribe( enCurso => {
      this.asistenciaEnCurso = enCurso;
    });
  }

  ngOnInit() { }

  logout() {
    this.autenticarService.logout();
  }

  loadLang(port) {
    window.location.href = `http://localhost:${port}`
    console.log(`change language to ${port}`);
  }

}
