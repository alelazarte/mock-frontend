export interface IUsuario {
  cuil?: string;
  nombre?: string;
  email?: string;
  clave?: string;
  habilitado?: boolean;
  verificado?: boolean;
}
