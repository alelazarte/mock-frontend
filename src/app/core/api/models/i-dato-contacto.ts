export interface IDatoContacto {
  tipo?: string;
  valor?: string;
  idEntidad?: number;
}
