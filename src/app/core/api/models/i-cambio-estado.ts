export interface ICambioEstado {
  idAsistencia?: number;
  nombreEstado?: string;
  fechaCambio?: Date;
}
