export interface IEntidad {
  id?: number;
  nombre?: string;
  habilitada?: boolean;
  fechaHabilitacion?: string;
}
