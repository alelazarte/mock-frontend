export interface IMensaje {
  idAsistencia?: number;
  cliente?: string;
  mensaje?: string;
  fecha?: Date;
}
