export interface IAsistencia {
  id?: number;
  cuil?: string;
  idEntidad?: number;
  geolocalizacion?: string;
  mensaje?: string;
  idEstado?: number;
  nombreEstado?: string;
  fechaSolicitada?: Date;
  nombreEntidad?: string;
  nombreUsuario?: string;
  finalizada?: boolean;
  cancelada?: boolean;
  archivos?: {}[];
}
