export interface IAdjunto {
  id?: number;
  idAsistencia?: number;
  pathPublico?: string;
  nombreArchivo?: string;
}
