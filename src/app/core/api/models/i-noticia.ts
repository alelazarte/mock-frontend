export interface INoticia {
  titulo?: string;
  autor?: string;
  cuerpo?: string;
  fechaPublicacion?: Date;
}
