export interface ISesion {
  cuil?: string;
  tokenSesion?: string;
  tokenVencimiento?: Date;
  nombre?: string;
}
