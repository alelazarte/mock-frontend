import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ResourceHandler, ResourceAction, ResourceParams, Resource, IResourceMethod, ResourceRequestMethod, ResourceRequestBodyType, ResourceResponseBodyType } from '@ngx-resource/core';
import { IEntidad } from '../models/i-entidad';
import { IAsistencia } from '../models/i-asistencia';
import { IUsuario } from '../models/i-usuario';
import { ISesion } from '../models/i-sesion';
import { INoticia } from '../models/i-noticia';
import { IMensaje } from '../models/i-mensaje';
import { ICambioEstado } from '../models/i-cambio-estado';
import { IDatoContacto } from '../models/i-dato-contacto';
import { IAdjunto } from '../models/i-adjunto';

@Injectable({
  providedIn: 'root'
})
@ResourceParams({
  pathPrefix: `${environment.apiUrl}`
})
export class AsistenciasResource extends Resource{

  constructor(requestHandler: ResourceHandler) { 
    super(requestHandler); 
  }

  @ResourceAction({
    path: '/entidades',
    method: ResourceRequestMethod.Get,
    requestBodyType: ResourceRequestBodyType.NONE,
    responseBodyType: ResourceResponseBodyType.Json
  })
  getEntidades: IResourceMethod<void, IEntidad[]>;

  @ResourceAction({
    path: '/entidades/{!id}',
    method: ResourceRequestMethod.Get,
    requestBodyType: ResourceRequestBodyType.JSON,
    responseBodyType: ResourceResponseBodyType.Json
  })
  datosContactoEntidad: IResourceMethod<{id: number}, IDatoContacto[]>;


  @ResourceAction({
    path: '/asistencias',
    method: ResourceRequestMethod.Post,
    requestBodyType: ResourceRequestBodyType.JSON
  })
  solicitarAsistencia: IResourceMethod<IAsistencia, number>;
  
  @ResourceAction({
    path: '/asistencias/estados/{!id}',
    method: ResourceRequestMethod.Get
  })
  cambioEstadoAsistencia: IResourceMethod<{id: number}, ICambioEstado[]>;

  @ResourceAction({
    path: '/asistencias/{!id}/adjuntos',
    method: ResourceRequestMethod.Get
  })
  getAsistenciaAdjuntos: IResourceMethod<{id: number}, IAdjunto[]>;


  @ResourceAction({
    path: '/asistencias/iniciar/{!id}',
    method: ResourceRequestMethod.Post
  })
  iniciarAsistencia: IResourceMethod<{id: number}, void>;
  
  @ResourceAction({
    path: '/asistencias/cancelar/{!id}',
    method: ResourceRequestMethod.Post
  })
  cancelarAsistencia: IResourceMethod<{id: number}, void>;

  @ResourceAction({
    path: '/asistencias/chat/{!id}',
    method: ResourceRequestMethod.Get
  })
  getChatHistorial: IResourceMethod<{id: number}, IMensaje[]>;

  @ResourceAction({
    path: '/asistencias/chat',
    method: ResourceRequestMethod.Post
  })
  enviarMensaje: IResourceMethod<IMensaje, void>;

  @ResourceAction({
    path: '/usuarios',
    method: ResourceRequestMethod.Post,
    requestBodyType: ResourceRequestBodyType.JSON,
    responseBodyType: ResourceResponseBodyType.Json
  })
  login: IResourceMethod<IUsuario, ISesion>;

  @ResourceAction({
    path: '/usuarios/validar',
    method: ResourceRequestMethod.Post,
    responseBodyType: ResourceResponseBodyType.Json
  })
  validarUsuario: IResourceMethod<{cuil: string, tokenSesion: string}, boolean>;

  @ResourceAction({
    path: '/usuarios',
    method: ResourceRequestMethod.Delete,
    requestBodyType: ResourceRequestBodyType.JSON
  })
  logout: IResourceMethod<{cuil: string}, void>;

  @ResourceAction({
    path: '/usuarios',
    method: ResourceRequestMethod.Put,
    requestBodyType: ResourceRequestBodyType.JSON
  })
  registrar: IResourceMethod<IUsuario, void>;

  @ResourceAction({
    path: '/usuarios/{!cuil}',
    method: ResourceRequestMethod.Get,
    requestBodyType: ResourceRequestBodyType.JSON
  })
  datosUsuario: IResourceMethod<{cuil: string}, IUsuario>;

  @ResourceAction({
    path: '/usuarios/verificar/{!cuil}',
    method: ResourceRequestMethod.Get
  })
  verificar: IResourceMethod<{cuil: string}, void>;

  @ResourceAction({
    path: '/asistencias/historial/{!cuil}',
    method: ResourceRequestMethod.Get
  })
  historial: IResourceMethod<{cuil: string}, IAsistencia[]>;

  @ResourceAction({
    path: '/asistencias/{!id}',
    method: ResourceRequestMethod.Get
  })
  getAsistencia: IResourceMethod<{id: number}, IAsistencia>;

  @ResourceAction({
    path: '/noticias',
    method: ResourceRequestMethod.Get,
    responseBodyType: ResourceResponseBodyType.Json
  })
  noticias: IResourceMethod<void, INoticia[]>;


}

@Injectable({
  providedIn: 'root'
})
export class AsistenciasService {

  constructor(private resource: AsistenciasResource) { }

  getEntidades(): Promise<any> {
    return this.resource.getEntidades();
  }

  solicitarAsistencia(asistencia: IAsistencia): Promise<any> {
    return this.resource.solicitarAsistencia(asistencia);
  }
}
