import { Injectable } from '@angular/core';
import { Message } from 'src/app/_models/message';
import { MatDialog } from '@angular/material';
import { MessageDialogComponent } from '../layout/message-dialog/message-dialog.component';

@Injectable()
export class MessagesService {

  constructor(private _dialog: MatDialog) { }

  public showMessage(message: Message): void {
    this._dialog.open(MessageDialogComponent, {
      data: message
    });
  }

}
