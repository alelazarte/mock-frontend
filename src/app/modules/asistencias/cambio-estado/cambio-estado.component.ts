import { Component, OnInit } from '@angular/core';
import { AsistenciasService } from 'src/app/_services/asistencias.service';
import { CambioEstado } from 'src/app/_models/cambio-estado';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cambio-estado',
  templateUrl: './cambio-estado.component.html',
  styleUrls: ['./cambio-estado.component.css']
})
export class CambioEstadoComponent implements OnInit {

  cambioEstados: CambioEstado[] = [{}];

  constructor(
    private asistenciasService: AsistenciasService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.asistenciasService.getCambioEstados(params['id']).subscribe(cambios => {
        this.cambioEstados = cambios;
      });
    });
  }

}
