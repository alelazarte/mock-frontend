import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearComponent } from './crear/crear.component';
import { HistorialComponent } from './historial/historial.component';
import { CancelarComponent } from './cancelar/cancelar.component';
import { ChatComponent } from './chat/chat.component';
import { StaticChatComponent } from './static-chat/static-chat.component';
import { CambioEstadoComponent } from './cambio-estado/cambio-estado.component';
import { AdjuntosComponent } from './adjuntos/adjuntos.component';
import { EnCurso } from 'src/app/_helpers/en-curso';
import { EnCursoComponent } from './en-curso/en-curso.component';


const routes: Routes = [
  { path: 'crear', component: CrearComponent },
  { path: 'historial', component: HistorialComponent, canActivate: [ EnCurso ] },
  { path: 'cancelar/:id', component: CancelarComponent },
  { path: 'chat/:id', component: ChatComponent },
  { path: 'static-chat/:id', component: StaticChatComponent },
  { path: 'estados/:id', component: CambioEstadoComponent },
  { path: ':id/adjuntos', component: AdjuntosComponent },
  { path: 'enCurso', component: EnCursoComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [ RouterModule ]
})
export class AsistenciasRoutingModule { }
