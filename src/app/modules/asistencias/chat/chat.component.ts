import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ChatService } from 'src/app/_services/chat.service';
import { Asistencia } from 'src/app/_models/asistencia';
import { Mensaje } from 'src/app/_models/mensaje';
import { AutenticarService } from 'src/app/_services/autenticar.service';
import { EnCursoComponent } from '../en-curso/en-curso.component';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  public historial: Mensaje[];
  public model: Mensaje = {};
  public asistencia: Asistencia = {};

  public chatCanal: Observable<number>;
  public chatSub: Subscription;

  public enviarFallido: boolean;
  public actualizarFallido: boolean;

  public mostrarAdjuntos = false;
  public mostrarInformacion = true;

  constructor(
    private autenticarService: AutenticarService,
    private chatService: ChatService,
    private enCursoComponent: EnCursoComponent,
  ) {
    this.enCursoComponent.asistencia$.subscribe( asistencia => {
      this.asistencia = asistencia;
      this.asistencia.nuevosMensajes = 0;
      this.historial = asistencia.historial;
      console.log(this.asistencia);
    });
    // traer todas las asistencias desde enCurso
    // forEach por cada una para consultar:
    // - finalizada
    // - historial
    // por otro lado, hacer this.historialActual = historial[id]
  }

  ngOnInit() {
    this.enviarFallido = false;
  }

  ngOnDestroy() {
  }

  onSubmit() {
    this.enviarFallido = false;
    if(this.model.mensaje === undefined || this.model.mensaje === '') {
      return;
    }
    this.historial.push({
      cliente: this.asistencia.nombreUsuario,
      mensaje: this.model.mensaje,
    });
    this.model.cliente = this.asistencia.cuil;
    this.model.idAsistencia = this.asistencia.id;
    this.chatService.enviarMensaje(this.model).subscribe(() => {
      this.model.mensaje = "";
      }, err => {
        this.enviarFallido = true;
        console.log(err);
        if(err.status === 401)
          this.autenticarService.logout();
      });
  }

  toggleMostrarAdjuntos() {
    this.mostrarAdjuntos = ! this.mostrarAdjuntos;
  }

  toggleMostrarInformacion() {
    this.mostrarInformacion = ! this.mostrarInformacion;
  }

  cancelar() {
    this.enCursoComponent.cancelar();
  }
}
