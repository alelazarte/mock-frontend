import { Component } from '@angular/core';
import { AsistenciasService } from 'src/app/_services/asistencias.service';
import { ActivatedRoute } from '@angular/router';
import { Adjunto } from 'src/app/_models/adjunto';
import { EnCursoComponent } from '../en-curso/en-curso.component';

@Component({
  selector: 'app-adjuntos',
  templateUrl: './adjuntos.component.html',
  styleUrls: ['./adjuntos.component.css']
})
export class AdjuntosComponent {

  adjuntos: Adjunto[];

  constructor(
    private asistenciasService: AsistenciasService,
    private route: ActivatedRoute,
    private enCursoComponent: EnCursoComponent,
  ) {
    this.enCursoComponent.chatId$.subscribe( id => {
      this.asistenciasService.getAdjuntos(id).subscribe(adjuntos => {
        this.adjuntos = adjuntos;
      }, err => {
        console.log(err);
      });
    });
  }

  goToLink(url: string){
    window.open(url, "_blank");
  }

}
