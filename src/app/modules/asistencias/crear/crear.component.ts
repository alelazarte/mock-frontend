import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AsistenciasService } from 'src/app/_services/asistencias.service';
import { AutenticarService } from 'src/app/_services/autenticar.service';
import { EntidadesService } from 'src/app/_services/entidades.service';
import { Asistencia } from 'src/app/_models/asistencia';
import { Router } from '@angular/router';
import { DatosContacto } from 'src/app/_models/datos-contacto';
import { Target } from '@angular/compiler';

@Component({
  selector: 'app-crear',
  templateUrl: './crear.component.html',
  styleUrls: ['./crear.component.css']
})
export class CrearComponent implements OnInit {

  entidades: {} = {};
  model: Asistencia = {};
  datosContacto: DatosContacto[] = [{}];
  archivos: {}[] = [];
  entidadEnCurso: boolean = false;
  errorSolicitar: boolean = false;
  entidadHabilitada: boolean = true;

  constructor(
    private autenticarService: AutenticarService,
    private asistenciasService: AsistenciasService,
    private entidadesService: EntidadesService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.errorSolicitar = false;
    this.entidadesService.getEntidades().subscribe(
      entidades => {
        entidades.forEach(e => {
          this.entidades[e.id] = e;
        });
      }, err => {
        console.log(err);
        if(err.status === 401)
          this.autenticarService.logout();
      });
  }

  eliminarArchivo(nombre: string) {
    this.archivos.splice(this.archivos.indexOf(nombre));
  }

  cargarArchivos(event) {
    if((event.target as HTMLInputElement).files.length === 0) {
      return;
    }
    const reader = new FileReader();
    reader.onload = () => {
      this.archivos.push({
        nombre: event.target.files[0].name,
        contenido: reader.result as string
      });
    };
    reader.readAsDataURL((event.target as HTMLInputElement).files[0]);
  }

  onSubmit() {
    this.model.cuil = this.autenticarService.usuarioValue.cuil;
    this.model.geolocalizacion = '-31.33787/-64.25663';
    this.model.archivos = this.archivos;
    this.asistenciasService.solicitar(this.model).subscribe(
      id => {
        this.asistenciasService.asistenciaEnCursoValue = true;
        this.router.navigate(['/']);
      }, err => {
        console.log(err);
        if(err.status === 401)
          this.autenticarService.logout();
        this.entidadesService.getDatosContacto(this.model.idEntidad).subscribe(
          datosContacto => {
            this.errorSolicitar = true;
            this.datosContacto = datosContacto;
          }, err => {
            console.log(err);
            if(err.status === 401)
              this.autenticarService.logout();
          });
      }
    );
  }

  revisarEntidadHabilitada() {
    this.errorSolicitar = false;
    if(! this.entidades[this.model.idEntidad].habilitada){
      this.entidadHabilitada = false;
      this.entidadesService.getDatosContacto(this.model.idEntidad).subscribe(
        datos => {
          this.datosContacto = datos;
        }, err => {
          console.log(err);
          if(err.status === 401)
            this.autenticarService.logout();
        });
    } else {
      this.entidadHabilitada = true;
    }
    this.entidadEnCurso = this.entidades[this.model.idEntidad].enCurso;
  }

}
