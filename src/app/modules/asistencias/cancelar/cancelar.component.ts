import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AsistenciasService } from 'src/app/_services/asistencias.service';
import { AutenticarService } from 'src/app/_services/autenticar.service';

@Component({
  selector: 'app-cancelar',
  templateUrl: './cancelar.component.html',
  styleUrls: ['./cancelar.component.css']
})
export class CancelarComponent implements OnInit {

  constructor(
    private asistenciasService: AsistenciasService,
    private autenticarService: AutenticarService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.asistenciasService.cancelar(params['id']).subscribe(() => {
        this.asistenciasService.asistenciaEnCursoValue = false;
        this.autenticarService.get(this.autenticarService.usuarioValue.cuil).subscribe(usuario => {
          console.log(usuario);
          if(! usuario.habilitado) {
            this.autenticarService.logout();
            this.router.navigate(['/sesion/inhabilitado']);
          } else {
            this.router.navigate(['/noticias']);
          }
        }, err => {
          console.log(err);
          if(err.status === 401)
            this.autenticarService.logout();
        });
      }, err => {
        console.log(err);
        if(err.status === 401)
          this.autenticarService.logout();
      });
    });
  }

}
