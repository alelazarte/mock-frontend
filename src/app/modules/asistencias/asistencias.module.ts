import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import {MatFormFieldModule} from '@angular/material/form-field'; 

import { AsistenciasRoutingModule } from './asistencias-routing.module';
import { CrearComponent } from './crear/crear.component';
import { HistorialComponent } from './historial/historial.component';
import { CancelarComponent } from './cancelar/cancelar.component';
import { ChatComponent } from './chat/chat.component';
import { CambioEstadoComponent } from './cambio-estado/cambio-estado.component';
import { AdjuntosComponent } from './adjuntos/adjuntos.component';
import { EnCursoComponent } from './en-curso/en-curso.component';
import { StaticChatComponent } from './static-chat/static-chat.component';


@NgModule({
  declarations: [CrearComponent, HistorialComponent, CancelarComponent, ChatComponent, CambioEstadoComponent, AdjuntosComponent, EnCursoComponent, StaticChatComponent],
  imports: [
    CommonModule,
    AsistenciasRoutingModule,
    FormsModule,
    MatFormFieldModule
  ]
})
export class AsistenciasModule { }
