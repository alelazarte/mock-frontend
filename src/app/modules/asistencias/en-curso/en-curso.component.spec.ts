import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnCursoComponent } from './en-curso.component';

describe('EnCursoComponent', () => {
  let component: EnCursoComponent;
  let fixture: ComponentFixture<EnCursoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnCursoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnCursoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
