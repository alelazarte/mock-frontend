import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, interval, Subscription } from 'rxjs';
import { AsistenciasService } from 'src/app/_services/asistencias.service';
import { Asistencia } from 'src/app/_models/asistencia';
import { AutenticarService } from 'src/app/_services/autenticar.service';
import { ChatService } from 'src/app/_services/chat.service';

@Component({
  selector: 'app-en-curso',
  templateUrl: './en-curso.component.html',
  styleUrls: ['./en-curso.component.css']
})
export class EnCursoComponent implements OnInit {

  public chatSeleccionado: boolean = false;
  public asistencias: Asistencia[];

  public chatCanal: Observable<number>;
  public chatSub: Subscription;

  private chatIdSource: BehaviorSubject<number>;
  public chatId$: Observable<number>;

  private asistenciaSource: BehaviorSubject<Asistencia>;
  public asistencia$: Observable<Asistencia>;

  private asistenciasSource: BehaviorSubject<Asistencia[]>;
  public asistencias$: Observable<Asistencia[]>;

  constructor(
    private asistenciasService: AsistenciasService,
    private autenticarService: AutenticarService,
    private chatService: ChatService,
    private router: Router,
  ) {
    this.chatIdSource = new BehaviorSubject<number>(0);
    this.chatId$ = this.chatIdSource.asObservable();

    this.asistenciasSource = new BehaviorSubject<Asistencia[]>(null);
    this.asistencias$ = this.asistenciasSource.asObservable();

    this.asistenciaSource = new BehaviorSubject<Asistencia>(null);
    this.asistencia$ = this.asistenciaSource.asObservable();
  }

  subNuevosMensajes() {
    this.chatCanal = interval(4000);

    this.chatSub = this.chatCanal.subscribe(x => {
      this.asistencias.forEach((a, i) => {
        if(a.finalizada || a.cancelada)
          return;
        this.chatService.historialNuevos(a.id).subscribe( historial => {
          console.log(`trabajando en asistencia id: ${a.id}`);
          // aca puedo hacer la logica para iluminar el chat con nvos mensajes
          // si historial.len > 0 && a.id != this.asistenciaValue.id = iluminar!
          if(historial.length > 0) {
            this.asistencias[i].historial = this.asistencias[i].historial.concat(historial);
            console.log(historial);
            console.log(this.asistencias[i].historial);
            if(a.id === this.asistenciaValue.id)
              this.asistenciaValue = this.asistencias[i];
            else
              this.asistencias[i].nuevosMensajes += historial.length;
            console.log(`la asistencia ${this.asistencias[i].id} tiene entonces ${this.asistencias[i].nuevosMensajes} nuevos mensajes`);
          }
          // actualizar asistenciaValue para reflejar cambios en chat

          this.chatService.isAsistenciaFinalizada(a.id).subscribe( finalizada => {
            if(finalizada) {
              this.asistenciasService.comprobarEnCurso(this.autenticarService.usuarioValue.cuil);
              this.asistencias[i].finalizada = true;
              this.asistencias[i].nombreEstado = 'Finalizada';

              if(a.id === this.asistenciaValue.id)
                this.asistenciaValue = this.asistencias[i];

            }
          }, err => {
            console.log(err);
            if(err.status === 401)
              this.autenticarService.logout();
            this.asistenciaValue.actualizarFallido = true;
          });
        }, err => {
          console.log(err);
          this.asistenciaValue.actualizarFallido = true;
          if(err.status === 401)
            this.autenticarService.logout();
        });
      });
    });
  }

  ngOnInit() {
    this.asistenciasService.getAsistenciasEnCurso(this.autenticarService.usuarioValue.cuil).subscribe(asistencias => {
      this.asistencias = asistencias;
      this.chatSeleccionado = true;
      this.asistenciaValue = asistencias[0];

      if(this.asistencias.length > 0) {
        // TODO borrar
        // this.chatIdValue = this.asistenciaValue.id;
        this.asistencias.forEach((a, i) => {
          if(a.finalizada || a.cancelada)
            return;
          this.chatService.historial(a.id).subscribe( historial => {
            this.asistencias[i].historial = historial;
            this.asistencias[i].nuevosMensajes = 0;
            if(a.id === this.asistenciaValue.id)
              this.asistenciaValue = this.asistencias[i];
          });
        });
      }

      this.subNuevosMensajes();
    }, err => {
      console.log(err);
      if(err.status === 401)
        this.autenticarService.logout();
    });

  }

  ngOnDestroy() {
    this.chatSub.unsubscribe();
  }

  public get chatIdValue() {
    return this.chatIdSource.value;
  }

  public set chatIdValue(id: number) {
    this.chatIdSource.next(id);
  }

  public get asistenciaValue() {
    return this.asistenciaSource.value;
  }

  public set asistenciaValue(asistencia: Asistencia) {
    this.asistenciaSource.next(asistencia);
  }

  cambiarChat(id: number) {
    this.chatSeleccionado = true;
    this.chatIdValue = id;
    this.asistenciaValue = this.getAsistenciaById(id);
  }

  getAsistenciaById(id: number): Asistencia {
    let asistencia: Asistencia = null;
    this.asistencias.forEach(a => {
      if(a.id === id)
        asistencia = a;
    });
    return asistencia;
  }

  cancelar() {
    // this.router.navigate([`/asistencias/cancelar/${this.asistencia.id}`]);
    this.asistenciasService.cancelar(this.asistenciaValue.id).subscribe(() => {
      this.asistenciasService.comprobarAsistenciasEnCurso(this.autenticarService.usuarioValue.cuil).subscribe( enCurso => {
        this.asistenciasService.asistenciaEnCursoValue = enCurso;
        this.autenticarService.get(this.autenticarService.usuarioValue.cuil).subscribe(usuario => {
          console.log(usuario);
          if(! usuario.habilitado) {
            this.ngOnDestroy();
            this.autenticarService.logout();
            this.router.navigate([`/sesion/login`], { queryParams: { inhabilitado: true }} );
          } else {
            this.asistenciaValue.cancelada = true;
            this.asistenciaValue.nombreEstado = 'Cancelada';
          }
        }, err => {
          console.log(err);
          if(err.status === 401)
            this.autenticarService.logout();
        });
      });
    }, err => {
      console.log(err);
      if(err.status === 401)
        this.autenticarService.logout();
    });
  }
}
