import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/_services/chat.service';
import { Mensaje } from 'src/app/_models/mensaje';
import { ActivatedRoute, Router } from '@angular/router';
import { AsistenciasService } from 'src/app/_services/asistencias.service';
import { Asistencia } from 'src/app/_models/asistencia';
import { Adjunto } from 'src/app/_models/adjunto';
import { AutenticarService } from 'src/app/_services/autenticar.service';

@Component({
  selector: 'app-static-chat',
  templateUrl: './static-chat.component.html',
  styleUrls: ['./static-chat.component.css']
})
export class StaticChatComponent implements OnInit {

  public chatId: number;

  public historial: Mensaje[];
  public adjuntos: Adjunto[];
  public asistencia: Asistencia = {};
  public actualizarFallido: boolean;

  public mostrarAdjuntos = false;
  public mostrarInformacion = true;

  constructor(
    private asistenciasService: AsistenciasService,
    private chatService: ChatService,
    private route: ActivatedRoute,
    private router: Router,
    private autenticarService: AutenticarService
  ) {
    this.route.params.subscribe(params => {
      this.chatId = params['id'];
    });
  }

  ngOnInit() {
    this.asistenciasService.get(this.chatId).subscribe( asistencia => {
        this.asistencia = asistencia;
      }, err => {
        console.log(err);
        this.actualizarFallido = true;
        if(err.status === 401)
          this.autenticarService.logout();
      });

    this.chatService.historial(this.chatId).subscribe( historial => {
      this.historial = historial;
    });

    this.asistenciasService.getAdjuntos(this.chatId).subscribe(adjuntos => {
      this.adjuntos = adjuntos;
    }, err => {
      console.log(err);
      this.actualizarFallido = true;
      if(err.status === 401)
        this.autenticarService.logout();
    });
  }

  toggleMostrarAdjuntos() {
    this.mostrarAdjuntos = ! this.mostrarAdjuntos;
  }

  toggleMostrarInformacion() {
    this.mostrarInformacion = ! this.mostrarInformacion;
  }
}
