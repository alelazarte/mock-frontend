import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AsistenciasService } from 'src/app/_services/asistencias.service';
import { Asistencia } from 'src/app/_models/asistencia';
import { AutenticarService } from '../../../_services/autenticar.service';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})
export class HistorialComponent implements OnInit {

  asistencias: Asistencia[] = [{}];

  constructor(
    private autenticarService: AutenticarService,
    private asistenciasService: AsistenciasService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.asistenciasService.historial(this.autenticarService.usuarioValue.cuil).subscribe(asistencias => {
      this.asistencias = asistencias;
      this.asistencias.forEach((a, i) => {
        this.asistenciasService.getCambioEstados(a.id).subscribe(cambios => {
          this.asistencias[i].cambioEstados = cambios;
        });
      });
    }, err => {
      console.log(err);
      if(err.status === 401)
        this.autenticarService.logout();
    });
    this.route.params.subscribe(params => {
    });
  }

}
