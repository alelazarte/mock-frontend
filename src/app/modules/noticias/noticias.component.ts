import { Component, OnInit } from '@angular/core';
import { NoticiasService } from 'src/app/_services/noticias.service';
import { Noticia } from 'src/app/_models/noticia';
import { AutenticarService } from 'src/app/_services/autenticar.service';

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.css']
})
export class NoticiasComponent implements OnInit {

  noticias: Noticia[];

  constructor(
    private noticiasService: NoticiasService,
    private autenticarService: AutenticarService,
  ) { }

  ngOnInit() {
    this.noticiasService.all().subscribe(noticias => {
      this.noticias = noticias;
    }, err => {
      console.log(err);
      if(err.status === 401)
        this.autenticarService.logout();
    });
  }

}
