import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Auth } from '../_helpers/auth';


const routes: Routes = [
  {
    path: '',
    loadChildren: './noticias/noticias.module#NoticiasModule',
    canActivate: [Auth]
  },
  {
    path: 'asistencias',
    loadChildren: './asistencias/asistencias.module#AsistenciasModule',
    canActivate: [Auth]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulesRoutingModule { }
