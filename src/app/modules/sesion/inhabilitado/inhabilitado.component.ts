import { Component, OnInit } from '@angular/core';
import { AutenticarService } from 'src/app/_services/autenticar.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inhabilitado',
  templateUrl: './inhabilitado.component.html',
  styleUrls: ['./inhabilitado.component.css']
})
export class InhabilitadoComponent implements OnInit {

  constructor(
    private router: Router,
    private autenticarService: AutenticarService
  ) { 
    this.autenticarService.logout();
    this.router.navigate([`/sesion/login`], { queryParams: { inhabilitado: true }} );
  }

  ngOnInit() { }

}
