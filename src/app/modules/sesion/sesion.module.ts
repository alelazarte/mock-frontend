import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SesionRoutingModule } from './sesion-routing.module';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegistrarComponent } from './registrar/registrar.component';
import { VerificarComponent } from './verificar/verificar.component';
import { InhabilitadoComponent } from './inhabilitado/inhabilitado.component';


@NgModule({
  declarations: [LoginComponent, LogoutComponent, RegistrarComponent, VerificarComponent, InhabilitadoComponent],
  imports: [
    CommonModule,
    SesionRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SesionModule { }
