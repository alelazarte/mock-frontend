import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { VerificarComponent } from './verificar/verificar.component';
import { InhabilitadoComponent } from './inhabilitado/inhabilitado.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'registrar', component: RegistrarComponent },
  { path: 'verificar/:cuil', component: VerificarComponent },
  { path: 'inhabilitado', component: InhabilitadoComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SesionRoutingModule { }
