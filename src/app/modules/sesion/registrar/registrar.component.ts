import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/_models/usuario';
import { Router } from '@angular/router';
import { FormBuilder, Form, FormGroup, FormControl, Validators } from '@angular/forms';
import { AutenticarService } from 'src/app/_services/autenticar.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {

  public model: Usuario;
  public form: FormGroup;
  public clave2: string;
  public email2: string;
  public verificando: boolean;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private autenticarService: AutenticarService,
  ) { 
    if(this.autenticarService.sesionValue !== null)
      this.router.navigate(['/noticias']);
    this.model = {};
    this.form = this.formBuilder.group({
      cuil: new FormControl(this.model.cuil, [
        Validators.required,
        Validators.minLength(13),
        Validators.maxLength(13),
        Validators.pattern('^[0-9 ]*$')
      ]),
      nombre: new FormControl(this.model.nombre, [
        Validators.required,
        Validators.minLength(3)
      ]),
      apellido: new FormControl(this.model.apellido, [
        Validators.required,
        Validators.minLength(3)
      ]),
      email: new FormControl(this.model.email, [
        Validators.email,
        Validators.required
      ]),
      clave: new FormControl(this.model.clave, [
        Validators.required,
        Validators.minLength(8)
      ])
    });

    this.form.get('cuil').valueChanges.subscribe( cuil => {
      let masked = '';
      for(var i = 0; i < cuil.length; i++) {
        if(! /[0-9 ]/.test(cuil[i]))
          continue;
        if( i == 2 && cuil[i] !== ' ')
          masked = `${masked} `
        if( i == 11 && cuil[i] !== ' ')
          masked = `${masked} `
        masked = `${masked}${cuil[i]}`
      }
      this.form.controls['cuil'].patchValue(masked, { emitEvent: false});
    });
  }

  get cuil() { return this.form.get('cuil'); }
  get nombre() { return this.form.get('nombre'); }
  get apellido() { return this.form.get('apellido'); }
  get email() { return this.form.get('email'); }
  get clave() { return this.form.get('clave'); }
  get confirmacionClave() { return this.form.get('confirmacionClave'); }
  get confirmacionEmail() { return this.form.get('confirmacionEmail'); }
  
  ngOnInit() {
    this.form.addControl('confirmacionClave', new FormControl(this.clave2, [
      (control: FormControl) => {
        if(control.value !== this.form.get('clave').value)
          return {error: 'Claves no coinciden'};
        return null;
      }
    ]));
    this.form.addControl('confirmacionEmail', new FormControl(this.email2, [
      (control: FormControl) => {
        if(control.value !== this.form.get('email').value)
          return {error: 'Emails no coinciden'};
        return null;
      }
    ]));
  }

  onSubmit() {
    if(this.form.invalid)
      return;
    
    this.model.cuil = this.form.get('cuil').value.replace(/ /g, '');
    this.model.nombre = this.form.get('nombre').value;
    this.model.apellido = this.form.get('apellido').value;
    this.model.email = this.form.get('email').value;
    this.model.clave = this.form.get('clave').value;

    this.autenticarService.registrar(this.model).subscribe(data => {
      this.verificando = true;
    }, err => {
      console.log(err);
    });
  }
}
