import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Sesion } from 'src/app/_models/sesion';
import { first } from 'rxjs/operators';
import { AsistenciasService } from 'src/app/_services/asistencias.service';
import { AutenticarService } from 'src/app/_services/autenticar.service';
import jwtDecode from 'jwt-decode';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {

  public form: FormGroup;
  public inhabilitado: boolean = false;
  public verificado: boolean = false;
  public logueando: boolean;
  public errorLogin: boolean = false;
  public mensajeError: string;

  constructor(
    private loginService: AutenticarService,
    private asistenciaService: AsistenciasService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private autenticarService: AutenticarService,
  ) { 
    if(this.autenticarService.sesionValue !== null)
      this.router.navigate(['/noticias']);
    this.route.queryParams.subscribe(params => {
      this.inhabilitado = params['inhabilitado'];
      this.verificado = params['verificado'];
    });
    this.form = this.formBuilder.group({
      cuil: new FormControl('', [
        Validators.required,
        Validators.minLength(11)
      ]),
      clave: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ])
    });

    this.form.get('cuil').valueChanges.subscribe( cuil => {
      let masked = '';
      for(var i = 0; i < cuil.length; i++) {
        if(! /[0-9 ]/.test(cuil[i]))
          continue;
        if( i == 2 && cuil[i] !== ' ')
          masked = `${masked} `
        if( i == 11 && cuil[i] !== ' ')
          masked = `${masked} `
        masked = `${masked}${cuil[i]}`
      }
      this.form.controls['cuil'].patchValue(masked, { emitEvent: false});
    });
  }

  get cuil() { return this.form.get('cuil'); }
  get clave() { return this.form.get('clave'); }

  onSubmit() {
    if(this.form.invalid)
      return;

    this.loginService.login(this.cuil.value.replace(/ /g, ''), this.clave.value).subscribe( tokenData => {
      this.autenticarService.sesionValue = tokenData.tokenSesion;
      this.loginService.get(this.cuil.value.replace(/ /g, '')).subscribe( data => {
        this.logueando = true;
        this.loginService.usuarioValue = data;
        this.asistenciaService.comprobarEnCurso(this.cuil.value.replace(/ /g, ''));
        this.router.navigate(['/noticias']);
      }, err => {
        this.mensajeError = err.error.message;
        this.errorLogin = true;
        console.log(err);
      });
    }, err => {
      this.mensajeError = err.error.message;
      this.errorLogin = true;
      console.log(err);
    });
  }

}
