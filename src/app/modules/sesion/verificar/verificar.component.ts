import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AutenticarService } from 'src/app/_services/autenticar.service';

@Component({
  selector: 'app-verificar',
  templateUrl: './verificar.component.html',
  styleUrls: ['./verificar.component.css']
})
export class VerificarComponent implements OnInit {

  constructor(
    private autenticarService: AutenticarService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.autenticarService.verificar(params['cuil']).subscribe(result => {
          console.log(result);
          this.router.navigate([`/sesion/login`], { queryParams: { verificado: true }} );
        },
        (err) => {
          console.log(err);
          this.router.navigate([`/sesion/login`], { queryParams: { error: true }} );
        }
      );
    });
  }

}
