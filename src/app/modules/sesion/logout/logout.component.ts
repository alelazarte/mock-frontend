import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AutenticarService } from 'src/app/_services/autenticar.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(
    private autenticarService: AutenticarService,
    private router: Router,
  ) { }

  ngOnInit() {
    // logout() deletes the session from local storage, no need to tell that to
    // the backend
    this.autenticarService.logout();
    this.router.navigate(['/sesion/login']);
  }

}
