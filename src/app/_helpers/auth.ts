import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AutenticarService } from 'src/app/_services/autenticar.service';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class Auth implements CanActivate {

  constructor(
    private router: Router,
    private autenticarService: AutenticarService,
  ){ }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if(this.autenticarService.sesionValue === null) {
      this.router.navigate([`/sesion/login`]);
      return false;
    }
    return this.autenticarService.sesionValue !== null;;
  }
}
