import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AsistenciasService } from '../_services/asistencias.service';
import { AutenticarService } from '../_services/autenticar.service';
import { Component, OnInit } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class EnCurso implements CanActivate, OnInit {

  constructor(
    private router: Router,
    private asistenciasService: AsistenciasService,
    private autenticarService: AutenticarService,
  ){ }

  ngOnInit() { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    this.asistenciasService.comprobarEnCurso(this.autenticarService.usuarioValue.cuil);

    if(this.asistenciasService.asistenciaEnCursoValue as boolean) {
      this.router.navigate([`/asistencias/enCurso`]);
    }
    // puede activar: false, si asistencia en curso: true
    // en otras palabras, no podes activar si hay asistencia en curso
    return !this.asistenciasService.asistenciaEnCursoValue;

  }

}
