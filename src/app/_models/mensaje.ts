export class Mensaje {
  idAsistencia?: number;
  cliente?: string;
  mensaje?: string;
  fecha?: Date;
}
