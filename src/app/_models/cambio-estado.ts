export class CambioEstado {
  idAsistencia?: number;
  nombreEstado?: string;
  fechaCambio?: Date;
}
