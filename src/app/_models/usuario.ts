export class Usuario {
  cuil?: string;
  nombre?: string;
  apellido?: string;
  email?: string;
  clave?: string;
  habilitado?: boolean;
  verificado?: boolean;
}
