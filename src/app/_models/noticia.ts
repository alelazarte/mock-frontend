export class Noticia {
  titulo?: string;
  autor?: string;
  cuerpo?: string;
  fuente?: string;
  fechaPublicacion?: Date;
}
