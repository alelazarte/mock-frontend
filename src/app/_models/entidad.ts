export class Entidad {
  id?: number;
  nombre?: string;
  habilitada?: boolean;
  fechaHabilitacion?: string;
}
