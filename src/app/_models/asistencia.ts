import { Mensaje } from 'src/app/_models/mensaje';
import { CambioEstado } from 'src/app/_models/cambio-estado';

export class Asistencia {
  id?: number;
  cuil?: string;
  idEntidad?: number;
  geolocalizacion?: string;
  mensaje?: string;
  idEstado?: number;
  nombreEstado?: string;
  fechaSolicitada?: Date;
  nombreEntidad?: string;
  nombreUsuario?: string;
  finalizada?: boolean;
  cancelada?: boolean;
  archivos?: {}[];
  historial?: Mensaje[];
  actualizarFallido?: boolean;
  nuevosMensajes?: number = 0;
  cambioEstados?: CambioEstado[];
}
