import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { LOCALE_ID, NgModule } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { MatListModule, MatSidenavModule, MatRippleModule } from '@angular/material';

import { HttpClient, HttpClientModule, HttpClientXsrfModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ResourceHandler } from '@ngx-resource/core';
import { ResourceModule } from '@ngx-resource/handler-ngx-http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { AppMocksResourceHandler } from './core/api/app-mocks-resource-handler';
import { MemoryDataService } from './core/mocks/memory-data.service';

import localeEs from '@angular/common/locales/es-419';

export function appHandlerFactory(http: HttpClient) {
  return new AppMocksResourceHandler(http);
}

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { HttpInterceptorService } from './core/interceptors/http-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'JSESSIONID',
      headerName: 'JSESSIONID'
    }),
    HttpClientInMemoryWebApiModule.forRoot(
      MemoryDataService, {dataEncapsulation: false, passThruUnknownUrl: true}
    ),
    ResourceModule.forRoot({
      handler: {provide: ResourceHandler, useFactory: (appHandlerFactory), deps: [HttpClient]}
    }),
    CoreModule,
    MatListModule,
    MatSidenavModule,
    MatRippleModule,
    AppRoutingModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'en_US' },
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor() {
    registerLocaleData(localeEs);
  }

}
