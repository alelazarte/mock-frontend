import { Injectable } from '@angular/core';
import { Entidad } from '../_models/entidad';
import { DatosContacto } from '../_models/datos-contacto';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AutenticarService } from 'src/app/_services/autenticar.service';

@Injectable({ providedIn: 'root' })
export class EntidadesService {

  constructor(
    private http: HttpClient,
    private autenticarService: AutenticarService
  ) { }

  getEntidades() {
    const headers = new HttpHeaders({
      'Authorization': `${this.autenticarService.sesionValue}`
    });
    return this.http.get<Entidad[]>(`${environment.apiUrl}/entidades`, { headers });
  }

  getDatosContacto(id) {
    const headers = new HttpHeaders({
      'Authorization': `${this.autenticarService.sesionValue}`
    });
    return this.http.get<DatosContacto[]>(`${environment.apiUrl}/entidades/${id}`, { headers });
  }
}
