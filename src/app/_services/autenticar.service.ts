import { Injectable } from '@angular/core';
import { Sesion } from '../_models/sesion';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Usuario } from 'src/app/_models/usuario';

@Injectable({ providedIn: 'root' })
export class AutenticarService {

  private sesionSub: BehaviorSubject<string>;
  public sesion$: Observable<string>;

  public usuario: Sesion;
  private usuarioSub: BehaviorSubject<Sesion>;
  public usuario$: Observable<Sesion>;

  public usuarioAutenticado: boolean = false;


  constructor(private http: HttpClient) {
    const sesion = JSON.parse(localStorage.getItem('sesion'));
    const usuario = JSON.parse(localStorage.getItem('usuario'));

    this.sesionSub = new BehaviorSubject<string>(sesion);
    this.sesion$ = this.sesionSub.asObservable();

    this.usuarioSub = new BehaviorSubject<Sesion>(usuario);
    this.usuario$ = this.usuarioSub.asObservable();
  }

  login(cuil, clave) {
    const headers = new HttpHeaders({
      'Authorization': `${cuil}:${clave}`
    });
    return this.http.post<any>(`${environment.apiUrl}/usuarios`, null, { headers });
  }

  logout() {
    localStorage.clear();
    this.usuarioAutenticado = false;
    this.sesionSub.next(null);
  }

  checkSesion() {
    this.sesionSub.next(localStorage.getItem('sesion'));
  }

  public get sesionValue(): string {
    return this.sesionSub.value;
  }

  public set sesionValue(sesion: string) {
    this.usuarioAutenticado = true;
    this.sesionSub.next(sesion);
    localStorage.setItem('sesion', JSON.stringify(sesion));
  }

  public get usuarioValue(): Sesion {
    return this.usuarioSub.value;
  }

  public set usuarioValue(usuario: Sesion) {
    localStorage.setItem('usuario', JSON.stringify(usuario));
    this.usuarioSub.next(usuario);
  }

  registrar(usuario: Usuario) {
    return this.http.put<any>(`${environment.apiUrl}/usuarios`, usuario);
  }

  verificar(cuil) {
    return this.http.get<any>(`${environment.apiUrl}/usuarios/verificar/${cuil}`);
  }

  get(cuil) {
    const headers = new HttpHeaders({
      'Authorization': `${this.sesionValue}`
    });
    return this.http.get<any>(`${environment.apiUrl}/usuarios/${cuil}`, { headers });
  }
}
