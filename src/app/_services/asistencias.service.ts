import { Injectable } from '@angular/core';
import { Asistencia } from '../_models/asistencia';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Adjunto } from 'src/app/_models/adjunto';
import { CambioEstado } from 'src/app/_models/cambio-estado';
import { HttpHeaders } from '@angular/common/http';
import { AutenticarService } from 'src/app/_services/autenticar.service';

@Injectable({ providedIn: 'root' })
export class AsistenciasService {

  private asistenciaEnCursoSource: BehaviorSubject<boolean>;
  public asistenciaEnCurso$: Observable<boolean>;

  private headers: HttpHeaders;

  constructor(
    private http: HttpClient,
    private autenticarService: AutenticarService
  ) { 
    const asistencia = localStorage.getItem('enCurso');
    let enCurso: boolean = false;
    if(asistencia != null)
      enCurso = asistencia == 'true';
    this.asistenciaEnCursoSource = new BehaviorSubject<boolean>(enCurso);
    this.asistenciaEnCurso$ = this.asistenciaEnCursoSource.asObservable();
  }

  solicitar(asistencia: Asistencia) {
    const headers = new HttpHeaders({
      'Authorization': `${this.autenticarService.sesionValue}`
    });
    return this.http.post<number>(`${environment.apiUrl}/asistencias`, asistencia, { headers });
  }

  cancelar(id) {
    const headers = new HttpHeaders({
      'Authorization': `${this.autenticarService.sesionValue}`
    });
    return this.http.post<any>(`${environment.apiUrl}/asistencias/cancelar/${id}`, null, { headers });
  }

  get(id) {
    const headers = new HttpHeaders({
      'Authorization': `${this.autenticarService.sesionValue}`
    });
    return this.http.get<Asistencia>(`${environment.apiUrl}/asistencias/${id}`, { headers });
  }

  isFinalizada(id) {
    const headers = new HttpHeaders({
      'Authorization': `${this.autenticarService.sesionValue}`
    });
    return this.http.get<boolean>(`${environment.apiUrl}/asistencias/isFinalizada/${id}`, { headers });
  }

  historial(cuil) {
    const headers = new HttpHeaders({
      'Authorization': `${this.autenticarService.sesionValue}`
    });
    return this.http.get<Asistencia[]>(`${environment.apiUrl}/asistencias/historial/${cuil}`, { headers });
  }

  getAsistenciasEnCurso(cuil) {
    const headers = new HttpHeaders({
      'Authorization': `${this.autenticarService.sesionValue}`
    });
    return this.http.get<Asistencia[]>(`${environment.apiUrl}/asistencias/enCurso/${cuil}`, { headers });
  }

  public get asistenciaEnCursoValue(): boolean {
    return this.asistenciaEnCursoSource.value;
  }

  public set asistenciaEnCursoValue(estado: boolean) {
    localStorage.setItem('enCurso', JSON.stringify(estado));
    this.asistenciaEnCursoSource.next(estado);
  }

  getAdjuntos(id) {
    const headers = new HttpHeaders({
      'Authorization': `${this.autenticarService.sesionValue}`
    });
    return this.http.get<Adjunto[]>(`${environment.apiUrl}/asistencias/${id}/adjuntos`, { headers });
  }

  getCambioEstados(id) {
    const headers = new HttpHeaders({
      'Authorization': `${this.autenticarService.sesionValue}`
    });
    return this.http.get<CambioEstado[]>(`${environment.apiUrl}/asistencias/estados/${id}`, { headers });
  }

  comprobarAsistenciasEnCurso(cuil) {
    const headers = new HttpHeaders({
      'Authorization': `${this.autenticarService.sesionValue}`
    });
    return this.http.get<boolean>(`${environment.apiUrl}/asistencias/comprobarEnCurso/${cuil}`, { headers });
  }

  comprobarEnCurso(cuil) {
    return this.comprobarAsistenciasEnCurso(cuil).subscribe(enCurso => {
      this.asistenciaEnCursoValue = enCurso;
    }, error => {
      this.asistenciaEnCursoValue = false;
    });
  }

}
