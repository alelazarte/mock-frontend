import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Noticia } from 'src/app/_models/noticia';
import { AutenticarService } from 'src/app/_services/autenticar.service';

@Injectable({ providedIn: 'root' })
export class NoticiasService {

  constructor(
    private http: HttpClient,
    private autenticarService: AutenticarService,
  ) { }

  all() {
    const headers = new HttpHeaders({
      'Authorization': `${this.autenticarService.sesionValue}`
    });
    return this.http.get<Noticia[]>(`${environment.apiUrl}/noticias`, { headers });
  }

}
