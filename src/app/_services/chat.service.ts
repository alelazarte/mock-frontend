import { Injectable } from '@angular/core';
import { Mensaje } from '../_models/mensaje';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpBackend, HttpHeaders } from '@angular/common/http';
import { AutenticarService } from 'src/app/_services/autenticar.service';

@Injectable({ providedIn: 'root' })
export class ChatService {

  private httpClient: HttpClient;

  constructor(
    private handler: HttpBackend,
    private autenticarService: AutenticarService
  ) {
    this.httpClient = new HttpClient(this.handler);
  }

  historial(id) {
    const headers = new HttpHeaders({
      'Authorization': `${this.autenticarService.sesionValue}`
    });
    return this.httpClient.get<Mensaje[]>(`${environment.apiUrl}/asistencias/chat/${id}`, { headers });
  }

  historialNuevos(id) {
    const headers = new HttpHeaders({
      'Authorization': `${this.autenticarService.sesionValue}`
    });
    return this.httpClient.get<Mensaje[]>(`${environment.apiUrl}/asistencias/chatNuevos/${id}`, { headers });
  }

  enviarMensaje(mensaje) {
    const headers = new HttpHeaders({
      'Authorization': `${this.autenticarService.sesionValue}`
    });
    return this.httpClient.post<Mensaje>(`${environment.apiUrl}/asistencias/chat`, mensaje, { headers });
  }

  // para que no use el http interceptor
  isAsistenciaFinalizada(id) {
    const headers = new HttpHeaders({
      'Authorization': `${this.autenticarService.sesionValue}`
    });
    return this.httpClient.get<boolean>(`${environment.apiUrl}/asistencias/isFinalizada/${id}`, { headers });
  }

}
